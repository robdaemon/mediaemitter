(function() {
  'use strict';

  var MediaEmitter = function() {
    this.initializeCastPlayer();
  };

  MediaEmitter.prototype.initializeCastPlayer = function() {
    if (!chrome.cast || !chrome.cast.isAvailable) {
      setTimeout(this.initializeCastApi.bind(this), 1000);
    }
  };

  MediaEmitter.prototype.initializeCastApi = function() {
    var sessionRequest = new chrome.cast.SessionRequest(
      chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID);
    
    var apiConfig = new chrome.cast.ApiConfig(sessionRequest,
      this.sessionListener.bind(this),
      this.receiverListener.bind(this));
    
    chrome.cast.initialize(apiConfig, this.onInitSuccess.bind(this), this.onError.bind(this));  
  };

  MediaEmitter.prototype.sessionListener = function(e) {
    console.log("sessionListener bound");
  };

  MediaEmitter.prototype.receiverListener = function(e) {
    var statusText = "";

    if('available' === e) {
      statusText = "Available!";
    } else {
      statusText = "Unavailable. Unable to find a Chromecast on your network.";
    }

    $("#castStatusAvailable").text(statusText);
    $("#castStatus").show();
  };

  MediaEmitter.prototype.onInitSuccess = function() {
    console.log("initialized cast API successfully");
  };

  MediaEmitter.prototype.onError = function() {
    console.log("error initializing cast API");
  };

  window.MediaEmitter = MediaEmitter;
})();
