Module: mediaemitter
Synopsis: 
Author: 
Copyright: 

define class <mediaemitter-page> (<dylan-server-page>)
end class <mediaemitter-page>;

define taglib mediaemitter ()  
end taglib mediaemitter;

define tag file-list in mediaemitter
  (page :: <dylan-server-page>)
  ()

  local method print-entry (dir :: <pathname>, filename :: <string>, type :: <file-type>)
          output("%s\n", filename);
        end;

  do-directory(print-entry, working-directory());
end tag file-list;

define function map-resources (server :: <http-server>)
  let home = make(<mediaemitter-page>, source: "dsp/index.dsp");
  add-resource(server, "/", home);

  let static-resource = make(<directory-resource>,
                             directory: "static",
                             allow-directory-listing?: #f);
  add-resource(server, "/static", static-resource);  
end function map-resources;

define function main () => ()
  http-server-main(server: make(<http-server>),
                   before-startup: map-resources);
end function main;

main();
