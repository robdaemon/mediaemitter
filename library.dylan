Module: dylan-user

define library mediaemitter
  use dylan;
  use common-dylan,
    import: { common-extensions };
  use io,
    import: { format, format-out, streams };
  use dsp;
  use http-common;
  use http-server;
  use system,
    import: { locators, threads, file-system };
end library;

define module mediaemitter
  use common-extensions,
    exclude: { format-to-string };
  use format-out;
  use dsp;
  use dylan;
  use format;
  use http-common;
  use http-server;
  use locators,
    exclude: { <http-server> };
  use streams;
  use threads;
  use file-system;
end module;
