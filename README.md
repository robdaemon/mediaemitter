# MediaEmitter

A simple application to send content from your local filesystem to your Chromecast. Or, what this will be as it gets further along.

## Requirements

- [Open Dylan 2013.2](http://opendylan.org/download/index.html)
- [Chromecast](https://play.google.com/store/devices/details?id=chromecast)

## Syncing the codebase

This uses submodules, and some nested submodules. Either clone with --recurse, or, do this afterwards:

`git submodule update --init --recursive`

## Compiling

Thanks to the submodules, you don't need to do anything really special to build.

```bash
dylan-compiler -build mediaemitter.lid
```

### Running

```bash
./_build/bin/mediaemitter -l 127.0.0.1:8080 --debug
```

You should change the -l parameter appropriately, based on what you want the http server to listen to.
